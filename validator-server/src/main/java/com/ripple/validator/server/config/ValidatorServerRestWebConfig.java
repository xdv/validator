package com.ripple.validator.server.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ripple.validator.common.config.JacksonConfig;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author Service Archetype
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.ripple.validator.server.rest.controllers")
@Import({JacksonConfig.class})
public class ValidatorServerRestWebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    @Qualifier("validator")
    private ObjectMapper objectMapper;

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        // Replace pre-existing MappingJackson2HttpMessageConverter with one using a custom ObjectMapper, preserving
        // order of existing convertrs.
        converters.replaceAll(converter -> {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                return new MappingJackson2HttpMessageConverter(objectMapper);
            } else {
                return converter;
            }
        });
    }
}
