package com.ripple.validator.server;

import com.ripple.runtime.server.ServerInitializer;

/**
 * @author Service Archetype
 */
public class ValidatorServerMain {

    public static void main(String[] args) {
        ServerInitializer.start(new ValidatorServer());
    }
}
