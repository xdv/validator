package com.ripple.validator.server.rest.controllers;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.ripple.libraries.server.rest.annotations.JsonRequestMapping;
import com.ripple.validator.api.ImmutableValidatorRecord;
import com.ripple.validator.api.ValidatorCancellationRequest;
import com.ripple.validator.api.ValidatorExecutionRequest;
import com.ripple.validator.api.ValidatorRecord;
import com.ripple.validator.impl.ValidatorImpl;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Service Archetype
 */
@RestController
@RequestMapping("/")
public class RootController {

    private static final Logger logger = LoggerFactory.getLogger(RootController.class);

    @Autowired
    private ValidatorImpl validator;

    @Autowired
    public RootController(ValidatorImpl validator) {
        this.validator = validator;
    }

    @JsonRequestMapping(method = GET, params = "id")
    public ValidatorRecord getValidatorPrototype(@RequestParam("id") long id) {
        return validator.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @JsonRequestMapping(method = GET, params = "correlation_id")
    public ValidatorRecord getValidatorPrototype(@RequestParam("correlation_id") UUID correlationId) {
        return validator.findByCorrelationId(correlationId).orElseThrow(ResourceNotFoundException::new);
    }

    @JsonRequestMapping(method = GET)
    public List<ValidatorRecord> getAllRecords() {
        return validator.findAll();
    }

    @JsonRequestMapping(method = POST)
    public ValidatorRecord postValidatorRecord(@RequestBody ValidatorRecord record) {
        logger.info("POSTing {}", record);
        return validator.save(record);
    }

    @JsonRequestMapping(path = "/execute", method = POST)
    public Optional<ValidatorRecord> postValidatorRecord(@RequestBody ValidatorExecutionRequest executionRequest) {
        logger.info("POSTing {}", executionRequest);
        return validator.executeCase(executionRequest);
    }

    @JsonRequestMapping(path = "/cancel", method = POST)
    public Optional<ValidatorRecord> postValidatorRecord(@RequestBody ValidatorCancellationRequest cancellationRequest) {
        logger.info("POSTing {}", cancellationRequest);
        return validator.cancelCase(cancellationRequest);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class ResourceNotFoundException extends RuntimeException {
    }
}
