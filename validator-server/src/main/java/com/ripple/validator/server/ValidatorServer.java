package com.ripple.validator.server;

import com.ripple.libraries.server.spring.SpringServer;
import com.ripple.validator.server.config.ValidatorServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Service Archetype
 */
public class ValidatorServer extends SpringServer {

    public ValidatorServer() {
        super(ValidatorServerConfig.class);
    }

    @Override
    protected Logger getLogger() {
        return LoggerFactory.getLogger(ValidatorServer.class);
    }
}
