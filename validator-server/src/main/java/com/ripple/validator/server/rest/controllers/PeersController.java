package com.ripple.validator.server.rest.controllers;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.ripple.libraries.server.rest.annotations.JsonRequestMapping;
import com.ripple.validator.api.ValidatorRecord;
import com.ripple.validator.client.ValidatorClientTemplate;
import com.ripple.validator.api.Peer;
import java.net.URI;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import okhttp3.HttpUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jfulton
 */
@RestController
@RequestMapping("/peers")
public class PeersController {

    private final ConcurrentHashMap<URI, Peer> peers = new ConcurrentHashMap<>();
    private final ValidatorClientTemplate clientTemplate;

    @Autowired
    public PeersController(ValidatorClientTemplate clientTemplate) {
        this.clientTemplate = clientTemplate;
    }

    @JsonRequestMapping
    public Collection<Peer> getPeers() {
        return peers.values();
    }

    @JsonRequestMapping(method = POST)
    public void addPeer(@RequestBody Peer peer) {
        peers.put(peer.getEndpoint(), peer);
    }

    @JsonRequestMapping(path = "/broadcast", method = POST)
    public void broadcast(@RequestBody ValidatorRecord record) {
        for (Peer peer : peers.values()) {
            clientTemplate.save(record, HttpUrl.get(peer.getEndpoint()));
        }
    }
}
