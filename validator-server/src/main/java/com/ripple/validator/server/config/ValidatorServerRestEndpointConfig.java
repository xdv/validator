package com.ripple.validator.server.config;

import static com.ripple.libraries.server.jetty.JettyUtils.createServlet;

import com.ripple.libraries.server.spring.EmbeddedServerServletContextListener;
import javax.annotation.PostConstruct;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.StatisticsHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * @author Service Archetype
 */
@Configuration
public class ValidatorServerRestEndpointConfig {

    private static final Logger logger = LoggerFactory.getLogger(ValidatorServerRestEndpointConfig.class);

    @Autowired
    private Environment env;

    @Bean(initMethod = "start", destroyMethod = "stop")
    @Qualifier("validator")
    public Server validatorJettyServer() throws Exception {
        final Server server = new Server();
        server.setHandler(statisticsHandler());
        server.addConnector(serverConnector(server));
        server.setStopTimeout(60_000);
        return server;
    }

    private ServerConnector serverConnector(Server server)
        throws Exception {
        ServerConnector connector = new ServerConnector(server);
        Integer port = env.getRequiredProperty("validator.server.port", Integer.class);
        Integer portOffset = env.getProperty("portOffset", Integer.class, 0);
        connector.setPort(port + portOffset);

        return connector;
    }

    /**
     * Wraps the servlet context handler with a handler that ensures graceful shutdown of in-flight requests.
     */
    private Handler statisticsHandler() {
        final StatisticsHandler statisticsHandler = new StatisticsHandler();
        statisticsHandler.setHandler(handlerCollection());
        return statisticsHandler;
    }

    private HandlerCollection handlerCollection() {
        HandlerCollection handlerCollection = new HandlerCollection();
        handlerCollection.addHandler(servletContextHandler());
        return handlerCollection;
    }

    /**
     * Configures all servlets and filters exposed on this Jetty instance.
     */
    private ServletContextHandler servletContextHandler() {
        final ServletContextHandler servletsHandler = new ServletContextHandler();
        servletsHandler.setContextPath("/");
        servletsHandler.addEventListener(validatorEmbeddedContextListener());
        servletsHandler
            .addServlet(createServlet("validator", dispatcherServlet(), ValidatorServerRestWebConfig.class), "/*");
        return servletsHandler;
    }

    private DispatcherServlet dispatcherServlet() {
        DispatcherServlet servlet = new DispatcherServlet();
        servlet.setThrowExceptionIfNoHandlerFound(true);
        return servlet;
    }

    /**
     * Ties the global Application-level spring context together with each Spring Servlet's sub-context in a
     * parent/child relationship.
     */
    @Bean
    public EmbeddedServerServletContextListener validatorEmbeddedContextListener() {
        return new EmbeddedServerServletContextListener();
    }

    @PostConstruct
    public void postConstruct() throws Exception {
        int port = ((ServerConnector) validatorJettyServer().getConnectors()[0]).getLocalPort();
        logger.info("Validator REST Endpoint started on port {}", port);
        System.setProperty("validator.local.rest.port", String.valueOf(port));
    }
}
