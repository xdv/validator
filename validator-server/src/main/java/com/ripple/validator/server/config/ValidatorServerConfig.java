package com.ripple.validator.server.config;

import com.ripple.spring.config.RuntimeModePropertySourceFactory;
import com.ripple.validator.client.config.ValidatorClientConfig;
import com.ripple.validator.impl.config.ValidatorImplConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Service Archetype
 */
@Configuration
@Import({
    ValidatorImplConfig.class,
    ValidatorServerRestEndpointConfig.class,
    ValidatorClientConfig.class,
})
@PropertySource(value = "classpath:/validator-server.properties", factory = RuntimeModePropertySourceFactory.class)
public class ValidatorServerConfig {

}
