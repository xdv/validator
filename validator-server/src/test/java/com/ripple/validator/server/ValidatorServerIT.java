package com.ripple.validator.server;

import com.ripple.libraries.spring.test.ServerManagerTestListener;
import com.ripple.validator.api.ImmutableValidatorCancellationRequest;
import com.ripple.validator.api.ImmutableValidatorExecutionRequest;
import com.ripple.validator.api.ImmutableValidatorRecord;
import com.ripple.validator.api.ValidatorCancellationRequest;
import com.ripple.validator.api.ValidatorExecutionRequest;
import com.ripple.validator.api.ValidatorRecord;
import com.ripple.validator.client.ValidatorClient;
import com.ripple.validator.client.config.ValidatorClientConfig;
import com.ripple.validator.server.ValidatorServerIT.TestListener;
import org.interledger.cryptoconditions.Condition;
import org.interledger.cryptoconditions.Fulfillment;
import org.interledger.cryptoconditions.PreimageSha256Fulfillment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Optional;
import java.util.UUID;

import static com.ripple.validator.common.states.PaymentState.COMPLETED;
import static com.ripple.validator.common.states.PaymentState.FAILED;
import static com.ripple.validator.common.states.PaymentState.PENDING;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Tests a client and server using, by default, a randomly generated server port discovered by the client.
 *
 * @author Service Archetype
 */
@ContextConfiguration(classes = {ValidatorClientConfig.class})
@TestExecutionListeners(listeners = {TestListener.class})
@DirtiesContext
public class ValidatorServerIT extends AbstractTestNGSpringContextTests {

    @Autowired
    private ValidatorClient client;

    /**
     * Tests {@link ValidatorClient#findByCorrelationId(UUID)}.
     */
    @Test
    public void testFindByCorrelationId() throws Exception {
        final UUID correlationId = UUID.fromString("d6c98947-d3ce-4eb0-9002-9797358ebcef");
        ValidatorRecord saved = client.save(ImmutableValidatorRecord.builder().correlationId(correlationId).build());
        assertThat(saved.id().isPresent(), is(true));
        Optional<ValidatorRecord> record = client.findByCorrelationId(correlationId);
        assertThat(record.isPresent(), is(true));
        assertThat(record.get().getState(), is(PENDING));
        assertThat(record.get().correlationId(), is(correlationId));
    }

    @Test
    public void testCreateCaseWithCondition() throws Exception {
        final UUID correlationId = UUID.fromString("f6c98947-d3ce-4eb0-9002-9797358ebcef");
        final Condition executionCondition = new PreimageSha256Fulfillment("execution".getBytes()).getCondition();
        ValidatorRecord saved = client.save(ImmutableValidatorRecord.builder()
            .correlationId(correlationId)
            .executionCondition(executionCondition)
            .build());
        assertThat(saved.id().isPresent(), is(true));
        assertThat(saved.getState(), is(PENDING));
        assertThat(saved.correlationId(), is(correlationId));
        assertThat(saved.executionCondition().get(), is(executionCondition));
        assertThat(saved.executionFulfillment().isPresent(), is(false));
    }

    @Test
    public void testExecuteCase() throws Exception {
        final UUID correlationId = UUID.fromString("cf5297e4-bdae-4d98-b112-d8385fab3b7c");
        client.save(ImmutableValidatorRecord.builder().correlationId(correlationId).build());

        ValidatorExecutionRequest executionRequest = ImmutableValidatorExecutionRequest.builder().correlationId(correlationId).build();

        Optional<ValidatorRecord> executed = client.executeCase(executionRequest);
        assertThat(executed.isPresent(), is(true));
        assertThat(executed.get().getState(), is(COMPLETED));
        assertThat(executed.get().correlationId(), is(correlationId));
    }

    @Test
    public void testExecuteCaseWithConditionFulfillment() throws Exception {
        final UUID correlationId = UUID.fromString("f8c98947-d3ce-4eb0-9002-9797358ebcef");
        final Fulfillment fulfillment = new PreimageSha256Fulfillment("execution".getBytes());
        final Condition executionCondition = fulfillment.getCondition();
        ValidatorRecord saved = client.save(ImmutableValidatorRecord.builder()
            .correlationId(correlationId)
            .executionCondition(executionCondition)
            .build());
        assertThat(saved.id().isPresent(), is(true));
        assertThat(saved.getState(), is(PENDING));
        assertThat(saved.correlationId(), is(correlationId));
        assertThat(saved.executionCondition().get(), is(executionCondition));
        assertThat(saved.executionFulfillment().isPresent(), is(false));

        ValidatorExecutionRequest executionRequest = ImmutableValidatorExecutionRequest.builder().correlationId(correlationId).fulfillment(fulfillment).build();
        Optional<ValidatorRecord> executed = client.executeCase(executionRequest);
        assertThat(executed.isPresent(), is(true));
        assertThat(executed.get().getState(), is(COMPLETED));
        assertThat(executed.get().correlationId(), is(correlationId));
    }

    @Test
    public void testCancelCase() throws Exception {
        final UUID correlationId = UUID.fromString("d558bb5c-9e07-4370-bed4-53575201a5c6");
        client.save(ImmutableValidatorRecord.builder().correlationId(correlationId).build());

        ValidatorCancellationRequest cancellationRequest = ImmutableValidatorCancellationRequest.builder().correlationId(correlationId).build();

        Optional<ValidatorRecord> cancelled = client.cancelCase(cancellationRequest);
        assertThat(cancelled.isPresent(), is(true));
        assertThat(cancelled.get().getState(), is(FAILED));
        assertThat(cancelled.get().correlationId(), is(correlationId));
    }

    /**
     * Starts a ValidatorServer before running any tests, and then shuts it down after all tests have completed.
     */
    public static class TestListener extends ServerManagerTestListener {
        public TestListener() {
            super(new ValidatorServer());
        }
    }
}
