package com.ripple.validator.common.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.NonTypedScalarSerializerBase;
import org.interledger.cryptoconditions.Condition;
import org.interledger.cryptoconditions.CryptoConditionReader;
import org.interledger.cryptoconditions.CryptoConditionWriter;
import org.interledger.cryptoconditions.der.DerEncodingException;

import java.io.IOException;
import java.util.Base64;

/**
 * @author matt
 */
public class CryptoConditionModule extends SimpleModule {

    private static final String NAME = "CryptoConditionModule";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {
    };

    public CryptoConditionModule() {
        super(NAME, VERSION_UTIL.version());
        addSerializer(Condition.class, new CryptoConditionUriSerializer());
        addDeserializer(Condition.class, new CryptoConditionUriDeserializer());
    }


    /**
     * Jackson Deserializer for Crypto {@link Condition} instances that reads a condition's URI and returns a {@link
     * Condition}.
     */
    public static class CryptoConditionUriDeserializer extends StdScalarDeserializer<Condition> {

        public CryptoConditionUriDeserializer() {
            super(String.class);
        }

        @Override
        public Condition deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            try {
                return CryptoConditionReader.readCondition(Base64.getUrlDecoder().decode(p.getText()));
            } catch (DerEncodingException e) {
                throw new RuntimeException(e);
            }
        }

    }

    /**
     * Jackson Serializer for Crypto {@link Condition} instances emits the condition's URI.
     */
    public static class CryptoConditionUriSerializer extends NonTypedScalarSerializerBase<Condition> {

        public CryptoConditionUriSerializer() {
            super(Condition.class, false);
        }

        @Override
        public void serialize(Condition value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            try {
                gen.writeString(Base64.getUrlEncoder().withoutPadding().encodeToString(CryptoConditionWriter.writeCondition(value)));
            } catch(DerEncodingException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
