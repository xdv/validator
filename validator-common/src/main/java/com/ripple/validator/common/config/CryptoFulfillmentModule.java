package com.ripple.validator.common.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.NonTypedScalarSerializerBase;
import org.interledger.cryptoconditions.CryptoConditionReader;
import org.interledger.cryptoconditions.CryptoConditionWriter;
import org.interledger.cryptoconditions.Fulfillment;
import org.interledger.cryptoconditions.der.DerEncodingException;

import java.io.IOException;
import java.util.Base64;

/**
 * @author matt
 */
public class CryptoFulfillmentModule extends SimpleModule {
    private static final String NAME = "CryptoConditionModule";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {
    };

    public CryptoFulfillmentModule() {
        super(NAME, VERSION_UTIL.version());
        addSerializer(Fulfillment.class, new CryptoFulfillmentUriSerializer());
        addDeserializer(Fulfillment.class, new CryptoFulfillmentUriDeserializer());
    }

    /**
     * Jackson Deserializer for Crypto {@link Fulfillment} instances that reads a fulfillment's base64 string and returns a {@link
     * Fulfillment}.
     */
    public static class CryptoFulfillmentUriDeserializer extends StdScalarDeserializer<Fulfillment> {

        public CryptoFulfillmentUriDeserializer() {
            super(String.class);
        }

        @Override
        public Fulfillment deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            try {
                return CryptoConditionReader.readFulfillment(Base64.getUrlDecoder().decode(p.getText()));
            } catch (DerEncodingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Jackson Serializer for Crypto {@link Fulfillment} instances emits the fulfillments base64 string representation.
     */
    public static class CryptoFulfillmentUriSerializer extends NonTypedScalarSerializerBase<Fulfillment> {

        public CryptoFulfillmentUriSerializer() {
            super(Fulfillment.class, false);
        }

        @Override
        public void serialize(Fulfillment fulfillment, JsonGenerator gen, SerializerProvider provider) throws IOException {
            try {
                gen.writeString(Base64.getUrlEncoder().withoutPadding().encodeToString(CryptoConditionWriter.writeFulfillment(fulfillment)));
            } catch (DerEncodingException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
