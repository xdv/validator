package com.ripple.validator.common.states;

/**
 * @author matt
 */
public enum PaymentState {
    PENDING,
    COMPLETED,
    FAILED
}
