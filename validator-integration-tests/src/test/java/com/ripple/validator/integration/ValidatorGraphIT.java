package com.ripple.validator.integration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import com.ripple.libraries.server.graph.Edge;
import com.ripple.libraries.server.graph.Graph;
import com.ripple.libraries.server.graph.Node;
import com.ripple.validator.api.ImmutablePeer;
import com.ripple.validator.api.ImmutableValidatorRecord;
import com.ripple.validator.api.Peer;
import com.ripple.validator.api.ValidatorRecord;
import com.ripple.validator.client.ValidatorClientTemplate;
import com.ripple.validator.client.config.ValidatorClientConfig;
import com.ripple.validator.server.ValidatorServer;
import okhttp3.HttpUrl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.UUID;

/**
 * @author Service Archetype
 */
public class ValidatorGraphIT {

    private static ValidatorClientTemplate CLIENT_TEMPLATE = new AnnotationConfigApplicationContext(
        ValidatorClientConfig.class).getBean(ValidatorClientTemplate.class);

    private static final String SOURCE = "validator01";
    private static final String DESTINATION = "validator02";

    private final Graph graph = new Graph();

    @Test
    public void testPeerBroadcasting() throws Exception {
        // Capture the existing number of records already on validator02
        int v02PreRecordCount = CLIENT_TEMPLATE.findAll(endpointFor(DESTINATION)).size();

        final UUID correlationId = UUID.fromString("6edabcac-d557-40a8-b3b9-a1e0c2a109e0");
        ValidatorRecord record = ImmutableValidatorRecord.builder().correlationId(correlationId).build();
        // Broadcast a record to validator02 via validator01
        CLIENT_TEMPLATE.broadcast(record, endpointFor(SOURCE));

        // Ensure that the number of records on validator02 has increased
        assertThat(CLIENT_TEMPLATE.findAll(endpointFor(DESTINATION)).size(), is(v02PreRecordCount + 1));
    }

    @BeforeClass
    public void setup() {
        graph
            .addNode(new Node(SOURCE, new ValidatorServer()))
            .addNode(new Node(DESTINATION, new ValidatorServer()))
            .addEdge(new PeeringEdge(SOURCE, DESTINATION))
            .startSync();
    }

    @AfterClass
    public void cleanup() {
        graph.stopSync();
    }

    private HttpUrl endpointFor(String serverKey) {
        return graph.getNode(serverKey).getEndpointUrl();
    }

    private class PeeringEdge extends Edge {
        private final String source;
        private final String destination;

        private PeeringEdge(String source, String destination) {
            this.source = source;
            this.destination = destination;
        }

        @Override
        public void connect(Graph graph) {
            Peer peer = ImmutablePeer.builder().endpoint(endpointFor(destination).uri()).build();
            CLIENT_TEMPLATE.addPeer(peer, endpointFor(source));
        }
    }
}
