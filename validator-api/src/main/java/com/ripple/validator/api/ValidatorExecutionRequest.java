package com.ripple.validator.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;
import org.interledger.cryptoconditions.Fulfillment;

import java.io.Serializable;
import java.util.Optional;
import java.util.UUID;

/**
 * @author matt
 */
@Value.Immutable
@JsonSerialize(as = ImmutableValidatorExecutionRequest.class)
@JsonDeserialize(as = ImmutableValidatorExecutionRequest.class)
public interface ValidatorExecutionRequest extends Serializable {
    @JsonProperty("correlation_id")
    UUID correlationId();

    Optional<Fulfillment> getFulfillment();
}
