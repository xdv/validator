package com.ripple.validator.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Optional;
import java.util.UUID;

import com.ripple.validator.common.states.PaymentState;
import org.immutables.value.Value;
import org.immutables.value.Value.Immutable;
import org.interledger.cryptoconditions.Condition;
import org.interledger.cryptoconditions.Fulfillment;

import static com.ripple.validator.common.states.PaymentState.PENDING;

/**
 * ValidatorRecord DTO.
 *
 * @author Service Archetype
 */
@Immutable
@JsonSerialize(as = ImmutableValidatorRecord.class)
@JsonDeserialize(as = ImmutableValidatorRecord.class)
public interface ValidatorRecord extends Serializable {
    Optional<Long> id();

    @JsonProperty("correlation_id")
    UUID correlationId();

    @JsonProperty("execution_condition")
    Optional<Condition> executionCondition();

    @JsonProperty("execution_fulfillment")
    Optional<Fulfillment> executionFulfillment();

    @Value.Default
    @JsonProperty("state")
    default PaymentState getState() {return PENDING;};
}
