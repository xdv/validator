package com.ripple.validator.api;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * The public facing interface implemented by clients and implementations.
 *
 * @author Service Archetype
 */
public interface Validator {

    /**
     * Creates a new {@link ValidatorRecord}.
     *
     * @param record record to save.
     * @return the record saved, including the server-assigned identifier.
     */
    ValidatorRecord save(ValidatorRecord record);

    /**
     * Returns a list of all {@link ValidatorRecord}s.
     * <p/>
     * Note: in a real system, you should never return a list of all items without properly paging the results.
     *
     * @return list of all {@link ValidatorRecord}s.
     */
    List<ValidatorRecord> findAll();

    /**
     * Finds a {@link ValidatorRecord} by id.
     *
     * @param id record id
     * @return an Optional of ValidatorRecord
     */
    Optional<ValidatorRecord> findById(long id);

    Optional<ValidatorRecord> findByCorrelationId(UUID correlationId);

    Optional<ValidatorRecord> executeCase(ValidatorExecutionRequest executionRequest);

    Optional<ValidatorRecord> cancelCase(ValidatorCancellationRequest cancellationRequest);
}
