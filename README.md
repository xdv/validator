
# Validator

==================
<dl>
  <dt>Application Name</dt><dd>validator</dd>
  <dt>Service Port</dt> <dd>9010</dd>
  <dt>Schema Name</dt><dd>validator</dd>
</dl>

=====================

Copy the following property and dependency into a consuming project's parent POM to manage this project's dependencies:

```xml
<properties>
    <com.ripple.validator.version>VERSION</com.ripple.validator.version>
</properties>

 <dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>com.ripple.validator</groupId>
            <artifactId>validator-bom</artifactId>
            <version>${com.ripple.validator.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

==========================

Must be invoked explicitly.  (Re)Creates the database schema and ensures the correct users are created and have
applicable permissions.  Invoke maven with this profile any time you'd like to set the database back to default
settings.  You should invoke maven with this profile the first time you check this project out to ensure there is a
schema available for the liquibase plugin.

Invoking:

```shell
$> mvn clean package -Pinitdb
```

Must be invoked explicitly. Applies the projects' database changesets to an existing schema, ensuring your
database always has the latest changes.  Liquibase runs automatically by default in TEST and DEV RuntimeModes any time
the server is started, so this profile is primarily used when it is desired to pre-populate a schema independent of
starting the server. 

Invoking:

```shell
$> mvn clean package -Pliquibase
```

Must be invoked explicitly.  Generates a migration.sql file containing the changes that would have been applied
with the liquibase profile. This is mostly used for applying changesets in production, allowing DBAs to review and
tweak before executing against the database server.  The resulting script can be found at
validator-persistence/target/liquibase/migrate.sql.

Invoking:

```shell
$> mvn clean package -Pliquibase-sql
```

Must be invoked explicitly. Assembles deployment bundles for any projects that require one.  Typically a bundle is a
tar.gz file containing appropriate libraries, resources, and scripts for starting and stopping the application. The
bundles can be found at validator-server/target/*.tar.gz.

Invoking:

```shell
$> mvn clean package -Ptarball
```

The maven exec plugin has been configured to allow running the server component of this project from the command
line.

Invoking:

```shell
$> cd validator-server
$> mvn exec:java

(Control-c to quit.)
```


Runtime Switches and Parameters
===========================
The runtime behavior of the server can be affected by supplying system properties at startup. Any property managed by
Spring can be overridden by passing a system property to the java command during start up. A system property can be
supplied in an IDE's VM options box in a run/debug profile.  It can be supplied on the bundle's service script, or using
mvn exec:java, as well.

Examples:

```shell
$> ./bin/service start -Dmy.property=foo
$> mvn exec:java -Dmy.property=foo
```


Valid values - (h2|mssql|postgres|oracle)

Allows for an explicit data store to be specified, overriding the RuntimeModes' default store.  An in-memory 'h2'
database is the default store in "TEST" RuntimeMode.  'postgres' is used by default in all other environments.  It can 
be useful to explicitly run the server using the h2 where it would otherwise run with a real database.

Invoking:

```shell
$> mvn exec:java -Ddb=h2
$> mvn exec:java -Ddb=postgres
```

Server Bundle
=============
The server bundle has the following structure:

```
/bin
   /service
   /service.settings
/lib
   /*.jar
/etc (optional)
   /(any external resources, such as overriding properties files)
/var
    service.pid
    /logs
        /app.log
        /err.log
        /app.*.log
        /err.*.log
```

The ./bin/service script starts and stops the server.  It uses applications-specific parameters defined in
./bin/service.settings.  This script can be executed from any directory, even if it has been symlinked. 

./bin/service has the following interface/commands:

Starts the server in the foreground in a blocking fashion.

Starts the server, in the background as a daemon, if it is not already running.

Stops the server, if it is running.

Stops the server if it is running, and then starts the server.

Displays the status of the server, and if it is running, what PID it is running under.

Displays the command that would be issued, the jars and directories that will be on the classpath, and the
environment that the process will run under.

Examples

```shell
$> ./bin/service start
$> ./bin/service stop
```

The ./bin/service.settings file contains the Main java class to be executed, the service name, and the JVM arguments
such as memory settings.
