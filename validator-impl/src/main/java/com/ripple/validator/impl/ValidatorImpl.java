package com.ripple.validator.impl;

import com.ripple.validator.api.ImmutableValidatorRecord;
import com.ripple.validator.api.Validator;
import com.ripple.validator.api.ValidatorCancellationRequest;
import com.ripple.validator.api.ValidatorExecutionRequest;
import com.ripple.validator.api.ValidatorRecord;
import com.ripple.validator.persistence.entities.ValidatorEntity;
import com.ripple.validator.persistence.repositories.ValidatorRepository;
import org.interledger.cryptoconditions.Condition;
import org.interledger.cryptoconditions.Fulfillment;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.ripple.validator.common.states.PaymentState.COMPLETED;
import static com.ripple.validator.common.states.PaymentState.FAILED;
import static com.ripple.validator.common.states.PaymentState.PENDING;

/**
 * @author Service Archetype
 */
@Transactional("validatorTM")
public class ValidatorImpl implements Validator {

    private final ValidatorRepository repository;

    public ValidatorImpl(ValidatorRepository repository) {
        this.repository = repository;
    }

    @Override
    public ValidatorRecord save(ValidatorRecord record) {
        ValidatorEntity entity = new ValidatorEntity(record.correlationId(), Instant.now()); // We don't care about expiry yet
        if (record.executionCondition().isPresent()) {
            entity.setExecutionCondition(record.executionCondition().get());
        }
        ValidatorEntity returned = repository.save(entity);
        return ImmutableValidatorRecord.builder()
            .correlationId(returned.getCorrelationId())
            .id(returned.getId())
            .state(returned.getState())
            .executionCondition(Optional.ofNullable(returned.getExecutionCondition()))
            .executionFulfillment(Optional.ofNullable(returned.getExecutionFulfillment()))
            .build();
    }

    @Override
    public Optional<ValidatorRecord> findById(long id) {
        Optional<ValidatorEntity> entity = repository.findById(id);
        return entity.map(e -> ImmutableValidatorRecord.builder()
            .correlationId(e.getCorrelationId())
            .state(e.getState())
            .id(e.getId())
            .executionCondition(Optional.ofNullable(e.getExecutionCondition()))
            .executionFulfillment(Optional.ofNullable(e.getExecutionFulfillment()))
            .build());
    }

    @Override
    public Optional<ValidatorRecord> findByCorrelationId(UUID correlationId) {
        Optional<ValidatorEntity> entity = repository.findByCorrelationId(correlationId);
        return entity.map(e -> ImmutableValidatorRecord.builder()
            .id(e.getId())
            .correlationId(e.getCorrelationId())
            .state(e.getState())
            .executionCondition(Optional.ofNullable(e.getExecutionCondition()))
            .executionFulfillment(Optional.ofNullable(e.getExecutionFulfillment()))
            .build());
    }

    @Override
    public Optional<ValidatorRecord> executeCase(ValidatorExecutionRequest executionRequest) {
        Optional<ValidatorEntity> entity = repository.findByCorrelationId(executionRequest.correlationId());
        if (entity.isPresent()) {
            assert(PENDING.equals(entity.get().getState()));
            if (executionRequest.getFulfillment().isPresent()) {
                assert(entity.get().getExecutionCondition() != null);
                final Fulfillment fulfillment = executionRequest.getFulfillment().get();
                final Condition condition = entity.get().getExecutionCondition();
                assert(fulfillment.verify(condition, new byte[0]));
                entity.get().setExecutionFulfillment(fulfillment);
            }

            entity.get().setState(COMPLETED);
            repository.save(entity.get());
            return entity.map(e -> ImmutableValidatorRecord.builder()
                .id(e.getId())
                .correlationId(e.getCorrelationId())
                .state(e.getState())
                .executionCondition(Optional.ofNullable(e.getExecutionCondition()))
                .executionFulfillment(Optional.ofNullable(e.getExecutionFulfillment()))
                .build());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<ValidatorRecord> cancelCase(ValidatorCancellationRequest cancellationRequest) {
        Optional<ValidatorEntity> entity = repository.findByCorrelationId(cancellationRequest.correlationId());
        if (entity.isPresent()) {
            assert(PENDING.equals(entity.get().getState()));

            entity.get().setState(FAILED);
            repository.save(entity.get());
            return entity.map(e -> ImmutableValidatorRecord.builder()
                .id(e.getId())
                .correlationId(e.getCorrelationId())
                .state(e.getState())
                .executionCondition(Optional.ofNullable(e.getExecutionCondition()))
                .executionFulfillment(Optional.ofNullable(e.getExecutionFulfillment()))
                .build());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<ValidatorRecord> findAll() {
        return repository
            .findAll()
            .stream()
            .map(e -> ImmutableValidatorRecord.builder()
                .correlationId(e.getCorrelationId())
                .id(e.getId())
                .state(e.getState())
                .executionCondition(Optional.ofNullable(e.getExecutionCondition()))
                .executionFulfillment(Optional.ofNullable(e.getExecutionFulfillment()))
                .build())
            .collect(Collectors.toList());
    }
}
