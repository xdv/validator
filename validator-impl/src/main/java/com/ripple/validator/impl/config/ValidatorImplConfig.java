package com.ripple.validator.impl.config;

import com.ripple.spring.config.RuntimeModePropertySourceFactory;
import com.ripple.validator.api.Validator;
import com.ripple.validator.impl.ValidatorImpl;
import com.ripple.validator.persistence.config.ValidatorPersistenceConfig;
import com.ripple.validator.persistence.repositories.ValidatorRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Service Archetype
 */
@Configuration
@Import({ValidatorPersistenceConfig.class})
@PropertySource(value = "classpath:/validator-impl.properties", factory = RuntimeModePropertySourceFactory.class)
public class ValidatorImplConfig {

    @Bean
    public Validator validator(ValidatorRepository repository) {
        return new ValidatorImpl(repository);
    }
}
