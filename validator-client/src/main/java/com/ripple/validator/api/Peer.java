package com.ripple.validator.api;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.net.URI;
import org.immutables.value.Value.Immutable;

/**
 * This type is only present to demonstrate an example of writing integration tests that dynamically configure multiple
 * servers to "talk" to each other.
 * <p/>
 * The typical workflow is to configure two or more server instances using a Graph, obtain the relevent endpoints,
 * and setting those endpoint on peers that will make calls to these endpoint.
 *
 * @author Service Archetype
 */
@Immutable
@JsonSerialize(as = ImmutablePeer.class)
@JsonDeserialize(as = ImmutablePeer.class)
public interface Peer {

    URI getEndpoint();
}
