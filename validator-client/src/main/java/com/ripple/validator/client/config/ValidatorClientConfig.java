package com.ripple.validator.client.config;

import static com.ripple.runtime.RuntimeMode.DEVELOPMENT_MODES;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ripple.validator.client.ValidatorClient;
import com.ripple.validator.client.ValidatorClientTemplate;
import com.ripple.validator.common.config.JacksonConfig;
import com.ripple.libraries.client.EndpointFactory;
import com.ripple.runtime.RuntimeMode;
import com.ripple.spring.config.RuntimeModePropertySourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * @author Service Archetype
 */
@Configuration
@Import({JacksonConfig.class})
@PropertySource(value = "classpath:/validator-client.properties", factory = RuntimeModePropertySourceFactory.class)
public class ValidatorClientConfig {

    @Autowired
    private Environment env;

    @Bean
    public ValidatorClient validatorClient(ValidatorClientTemplate clientTemplate) {
        return new ValidatorClient(clientTemplate, new EndpointFactory(env).restEndpoint("validator"));
    }

    @Bean
    public ValidatorClientTemplate validatorClientTemplate(@Qualifier("validatorClient") RestTemplate restTemplate) {
        return new ValidatorClientTemplate(restTemplate);
    }

    @Bean
    @Qualifier("validatorClient")
    public RestTemplate validatorClientRestTemplate(@Qualifier("validator") ObjectMapper objectMapper) {
        OkHttp3ClientHttpRequestFactory requestFactory = new OkHttp3ClientHttpRequestFactory();
        if (RuntimeMode.getMode().isIn(DEVELOPMENT_MODES)) {
            requestFactory.setReadTimeout(1000 * 60 * 5);
            requestFactory.setWriteTimeout(1000 * 60 * 5);
            requestFactory.setConnectTimeout(1000 * 60 * 5);
        }
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        restTemplate.getMessageConverters().replaceAll(messageConverter -> {
            if (messageConverter instanceof MappingJackson2HttpMessageConverter) {
                return new MappingJackson2HttpMessageConverter(objectMapper);
            } else {
                return messageConverter;
            }
        });
        return restTemplate;
    }
}
