package com.ripple.validator.client;

import com.ripple.validator.api.Peer;
import com.ripple.validator.api.ValidatorCancellationRequest;
import com.ripple.validator.api.ValidatorExecutionRequest;
import com.ripple.validator.api.ValidatorRecord;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import okhttp3.HttpUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author Service Archetype
 */
public class ValidatorClientTemplate {

    private static final Logger logger = LoggerFactory.getLogger(ValidatorClientTemplate.class);
    private final RestTemplate restTemplate;

    public ValidatorClientTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ValidatorRecord save(ValidatorRecord record, HttpUrl endpoint) {
        HttpEntity<ValidatorRecord> entity = new HttpEntity<>(record);
        return restTemplate.postForEntity(endpoint.uri(), entity, ValidatorRecord.class).getBody();
    }

    public Optional<ValidatorRecord> findById(long id, HttpUrl endpoint) {
        URI uri = endpoint.newBuilder().addQueryParameter("id", String.valueOf(id)).build().uri();
        ResponseEntity<ValidatorRecord> responseEntity = restTemplate.getForEntity(uri, ValidatorRecord.class);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return Optional.of(responseEntity.getBody());
        } else {
            return Optional.empty();
        }
    }

    public Optional<ValidatorRecord> findByCorrelationId(UUID correlationId, HttpUrl endpoint) {
        URI uri = endpoint.newBuilder().addQueryParameter("correlation_id", correlationId.toString()).build().uri();
        ResponseEntity<ValidatorRecord> responseEntity = restTemplate.getForEntity(uri, ValidatorRecord.class);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return Optional.of(responseEntity.getBody());
        } else {
            return Optional.empty();
        }
    }

    public Optional<ValidatorRecord> executeCase(ValidatorExecutionRequest executionRequest, HttpUrl endpoint) {
        URI uri = endpoint.newBuilder("execute").build().uri();
        ResponseEntity<ValidatorRecord> responseEntity = restTemplate.postForEntity(uri, executionRequest, ValidatorRecord.class);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return Optional.of(responseEntity.getBody());
        } else {
            return Optional.empty();
        }
    }

    public Optional<ValidatorRecord> cancelCase(ValidatorCancellationRequest cancellationRequest, HttpUrl endpoint) {
        URI uri = endpoint.newBuilder("cancel").build().uri();
        ResponseEntity<ValidatorRecord> responseEntity = restTemplate.postForEntity(uri, cancellationRequest, ValidatorRecord.class);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return Optional.of(responseEntity.getBody());
        } else {
            return Optional.empty();
        }
    }

    public List<ValidatorRecord> findAll(HttpUrl endpoint) {
        ParameterizedTypeReference<List<ValidatorRecord>> typeRef = new ParameterizedTypeReference<List<ValidatorRecord>>() {};
        return restTemplate.exchange(endpoint.uri(), HttpMethod.GET, null, typeRef).getBody();
    }

    /**
     * Used to demonstrate dynamically configured integration tests.
     *
     * @see Peer
     * @param peer
     * @param endpoint
     */
    public void addPeer(Peer peer, HttpUrl endpoint) {
        HttpEntity<Peer> entity = new HttpEntity<>(peer);
        restTemplate.postForLocation(endpoint.newBuilder("peers").build().uri(), entity);
    }

    /**
     * Used to demonstrate dynamically configured integration tests.
     *
     * @see Peer
     * @param endpoint
     */
    public void broadcast(ValidatorRecord record, HttpUrl endpoint) {
        HttpEntity<ValidatorRecord> entity = new HttpEntity<>(record);
        restTemplate.postForLocation(endpoint.newBuilder("peers/broadcast").build().uri(), entity);
    }
}
