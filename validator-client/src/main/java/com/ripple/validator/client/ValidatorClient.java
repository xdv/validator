package com.ripple.validator.client;

import com.ripple.validator.api.ValidatorCancellationRequest;
import com.ripple.validator.api.ValidatorExecutionRequest;
import com.ripple.validator.api.ValidatorRecord;
import com.ripple.validator.api.Validator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import okhttp3.HttpUrl;

/**
 * @author Service Archetype
 */
public class ValidatorClient implements Validator {

    private final ValidatorClientTemplate clientTemplate;
    private final HttpUrl endpoint;

    public ValidatorClient(ValidatorClientTemplate clientTemplate, HttpUrl endpoint) {
        this.clientTemplate = clientTemplate;
        this.endpoint = endpoint;
    }

    @Override
    public ValidatorRecord save(ValidatorRecord record) {
        return clientTemplate.save(record, endpoint);
    }

    @Override
    public Optional<ValidatorRecord> findById(long id) {
        return clientTemplate.findById(id, endpoint);
    }

    @Override
    public Optional<ValidatorRecord> executeCase(ValidatorExecutionRequest executionRequest) {
        return clientTemplate.executeCase(executionRequest, endpoint);
    }

    @Override
    public Optional<ValidatorRecord> cancelCase(ValidatorCancellationRequest cancellationRequest) {
        return clientTemplate.cancelCase(cancellationRequest, endpoint);
    }

    @Override
    public List<ValidatorRecord> findAll() {
        return clientTemplate.findAll(endpoint);
    }

    @Override
    public Optional<ValidatorRecord> findByCorrelationId(UUID correlationId) {
        return clientTemplate.findByCorrelationId(correlationId, endpoint);
    }
}
