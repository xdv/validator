package com.ripple.validator.persistence.entities;

import org.interledger.cryptoconditions.Condition;
import org.interledger.cryptoconditions.PreimageSha256Fulfillment;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.UUID;

import static com.ripple.validator.common.states.PaymentState.PENDING;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author matt
 */
public class ValidatorEntityTest {
    private static final UUID CORRELATION_ID = UUID.fromString("8165105f-ffdf-4946-9730-0356f7f912a0");
    private static final Instant EXPIRES_DTTM = Instant.now();
    private static final Condition EXECUTION_CONDITION = new PreimageSha256Fulfillment("execution".getBytes()).getCondition();

    @Test
    public void testGetters() throws Exception {
        final ValidatorEntity validatorEntity = new ValidatorEntity(CORRELATION_ID, EXPIRES_DTTM);

        assertThat(validatorEntity.getCorrelationId(), is(CORRELATION_ID));
        assertThat(validatorEntity.getId(), is(nullValue()));
        assertThat(validatorEntity.getExpiresDttm(), is(EXPIRES_DTTM));
        assertThat(validatorEntity.getState(), is(PENDING));
        assertThat(validatorEntity.getExecutionCondition(), is(nullValue()));
        assertThat(validatorEntity.getExecutionFulfillment(), is(nullValue()));
    }

    @Test
    public void testSetExecutionCondition() throws Exception {
        final Condition executionCondition = new PreimageSha256Fulfillment("execution".getBytes()).getCondition();
        final ValidatorEntity validatorEntity = new ValidatorEntity(CORRELATION_ID, EXPIRES_DTTM);
        validatorEntity.setExecutionCondition(executionCondition);

        assertThat(validatorEntity.getCorrelationId(), is(CORRELATION_ID));
        assertThat(validatorEntity.getId(), is(nullValue()));
        assertThat(validatorEntity.getExpiresDttm(), is(EXPIRES_DTTM));
        assertThat(validatorEntity.getState(), is(PENDING));
        assertThat(validatorEntity.getExecutionCondition(), is(executionCondition));
        assertThat(validatorEntity.getExecutionFulfillment(), is(nullValue()));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testNullCorrelationId() throws Exception {
        try {
            final ValidatorEntity validatorEntity = new ValidatorEntity(null, EXPIRES_DTTM);
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is ("correlationId may not be null"));
            throw e;
        }
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testNullExpiresDttm() throws Exception {
        try {
            final ValidatorEntity validatorEntity = new ValidatorEntity(CORRELATION_ID, null);
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is ("expiresDttm may not be null"));
            throw e;
        }
    }
}