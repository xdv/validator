package com.ripple.validator.persistence.repositories;

import com.ripple.validator.persistence.config.ValidatorPersistenceConfig;
import com.ripple.validator.persistence.entities.ValidatorEntity;
import org.interledger.cryptoconditions.Condition;
import org.interledger.cryptoconditions.PreimageSha256Fulfillment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static com.ripple.validator.common.states.PaymentState.PENDING;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Service Archetype
 */
@ContextConfiguration(classes = {ValidatorPersistenceConfig.class})
@Transactional
public class ValidatorRepositoryTest extends AbstractTransactionalTestNGSpringContextTests {

    private static final long NON_EXISTENT_VALIDATOR_RECORD_ID = 10_000_000;
    private static final UUID CORRELATION_ID = UUID.fromString("8165105f-ffdf-4946-9730-0356f7f912a0");
    private static final Instant EXPIRES_DTTM = Instant.now();
    private static final Condition EXECUTION_CONDITION = new PreimageSha256Fulfillment("execution".getBytes()).getCondition();

    @Autowired
    private ValidatorRepository repository;

    @Test
    public void testFindById_NonExistentEntity() throws Exception {
        assertThat(repository.findById(NON_EXISTENT_VALIDATOR_RECORD_ID).isPresent(), is(false));
    }

    @Test
    public void testFindById() throws Exception {
        ValidatorEntity entity = new ValidatorEntity(CORRELATION_ID, EXPIRES_DTTM);
        assertThat(entity.getId(), is(nullValue()));

        entity = repository.save(entity);
        assertThat(entity.getId(), is(notNullValue()));

        Optional<ValidatorEntity> selected = repository.findById(entity.getId());
        assertThat(selected.isPresent(), is(true));;
        assertThat(selected.get().getId(), is(entity.getId()));
        assertThat(selected.get().getCorrelationId(), is(CORRELATION_ID));
        assertThat(selected.get().getState(), is(PENDING));
        assertThat(selected.get().getExpiresDttm(), is(EXPIRES_DTTM));
    }

    @Test
    public void testFindByCorrelationId() throws Exception {
        ValidatorEntity entity = new ValidatorEntity(CORRELATION_ID, EXPIRES_DTTM);
        entity.setExecutionCondition(EXECUTION_CONDITION);
        assertThat(entity.getId(), is(nullValue()));

        entity = repository.save(entity);
        assertThat(entity.getId(), is(notNullValue()));

        Optional<ValidatorEntity> selected = repository.findByCorrelationId(entity.getCorrelationId());
        assertThat(selected.isPresent(), is(true));;
        assertThat(selected.get().getId(), is(entity.getId()));
        assertThat(selected.get().getCorrelationId(), is(CORRELATION_ID));
        assertThat(selected.get().getState(), is(PENDING));
        assertThat(selected.get().getExpiresDttm(), is(EXPIRES_DTTM));
        assertThat(selected.get().getExecutionCondition(), is(EXECUTION_CONDITION));
        assertThat(selected.get().getExecutionFulfillment(), is(nullValue()));
    }
}