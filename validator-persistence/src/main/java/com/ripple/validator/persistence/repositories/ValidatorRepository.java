package com.ripple.validator.persistence.repositories;

import com.ripple.validator.persistence.entities.ValidatorEntity;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Service Archetype
 */
@Repository
public interface ValidatorRepository extends JpaRepository<ValidatorEntity, Long>, QueryDslPredicateExecutor<ValidatorEntity> {

    Optional<ValidatorEntity> findById(Long id);

    Optional<ValidatorEntity> findByCorrelationId(UUID correlationId);
}
