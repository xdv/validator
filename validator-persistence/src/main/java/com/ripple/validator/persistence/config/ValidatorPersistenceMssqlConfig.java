package com.ripple.validator.persistence.config;

import static com.ripple.runtime.RuntimeDatabase.MSSQL;

import com.ripple.runtime.Switches;
import com.ripple.spring.annotations.ConditionalOnDatabase;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 * @author Service Archetype
 */
@Configuration
@ConditionalOnDatabase(MSSQL)
public class ValidatorPersistenceMssqlConfig {

    private static final Logger logger = LoggerFactory.getLogger(ValidatorPersistenceMssqlConfig.class);

    private final Environment env;

    @Autowired
    public ValidatorPersistenceMssqlConfig(Environment env) {
        this.env = env;
    }

    @Bean
    public DataSource validatorDS() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPoolName("validator");
        logger.info("Configuring Validator Persistence with MSSQL database");
        dataSource.setJdbcUrl(env.getRequiredProperty("validator.db.mssql.url"));
        dataSource.setUsername(env.getRequiredProperty("validator.db.mssql.user"));
        dataSource.setPassword(env.getRequiredProperty("validator.db.mssql.password"));
        return dataSource;
    }

    @Bean
    public HibernateJpaVendorAdapter validatorVA() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.SQL_SERVER);
        vendorAdapter.setShowSql(Switches.showSql.isEnabled());
        return vendorAdapter;
    }
}
