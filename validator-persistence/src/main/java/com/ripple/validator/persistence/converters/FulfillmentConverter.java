package com.ripple.validator.persistence.converters;

import org.interledger.cryptoconditions.CryptoConditionReader;
import org.interledger.cryptoconditions.CryptoConditionWriter;
import org.interledger.cryptoconditions.Fulfillment;
import org.interledger.cryptoconditions.der.DerEncodingException;

import javax.persistence.AttributeConverter;

/**
 * @author matt
 */
public class FulfillmentConverter implements AttributeConverter<Fulfillment, byte[]> {
    @Override
    public byte[] convertToDatabaseColumn(Fulfillment fulfillment) {
        if (fulfillment == null) return null;
        try {
            return CryptoConditionWriter.writeFulfillment(fulfillment);
        } catch (DerEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Fulfillment convertToEntityAttribute(byte[] dbValue) {
        if (dbValue == null) return null;
        try {
            return CryptoConditionReader.readFulfillment(dbValue);
        } catch(DerEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
