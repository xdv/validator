package com.ripple.validator.persistence.config;

import static com.ripple.runtime.RuntimeDatabase.ORACLE;

import com.ripple.runtime.Switches;
import com.ripple.spring.annotations.ConditionalOnDatabase;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 * @author Service Archetype
 */
@Configuration
@ConditionalOnDatabase(ORACLE)
public class ValidatorPersistenceOracleConfig {

    private static final Logger logger = LoggerFactory.getLogger(ValidatorPersistenceOracleConfig.class);

    private final Environment env;

    @Autowired
    public ValidatorPersistenceOracleConfig(Environment env) {
        this.env = env;
    }

    @Bean
    public DataSource validatorDS() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPoolName("validator");
        logger.info("Configuring Validator Persistence with Oracle database");
        dataSource.setJdbcUrl(env.getRequiredProperty("validator.db.oracle.url"));
        dataSource.setUsername(env.getRequiredProperty("validator.db.oracle.user"));
        dataSource.setPassword(env.getRequiredProperty("validator.db.oracle.password"));
        return dataSource;
    }

    @Bean
    public HibernateJpaVendorAdapter validatorVA() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.ORACLE);
        vendorAdapter.setShowSql(Switches.showSql.isEnabled());
        return vendorAdapter;
    }
}
