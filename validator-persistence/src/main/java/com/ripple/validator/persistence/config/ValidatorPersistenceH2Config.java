package com.ripple.validator.persistence.config;

import static com.ripple.runtime.RuntimeDatabase.H2;

import com.ripple.runtime.Switches;
import com.ripple.spring.annotations.ConditionalOnDatabase;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 * @author Service Archetype
 */
@Configuration
@ConditionalOnDatabase(H2)
public class ValidatorPersistenceH2Config {

    private static final Logger logger = LoggerFactory.getLogger(ValidatorPersistenceH2Config.class);

    @Bean
    public DataSource validatorDS() {
        logger.info("Configuring Validator Persistence with H2 database");
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPoolName("validator");
        dataSource.setDataSource(new EmbeddedDatabaseBuilder()
            .generateUniqueName(true)
            .setType(EmbeddedDatabaseType.H2)
            .addScript("db/h2-init.sql").build());
        return dataSource;
    }

    @Bean
    public HibernateJpaVendorAdapter validatorVA() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.POSTGRESQL);
        vendorAdapter.setShowSql(Switches.showSql.isEnabled());
        return vendorAdapter;
    }

}
