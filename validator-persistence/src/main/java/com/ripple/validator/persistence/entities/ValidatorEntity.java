package com.ripple.validator.persistence.entities;

import com.ripple.persistence.jpa.entities.AbstractCreatedModifiedVersioned;
import com.ripple.persistence.jpa.entities.Identifiable;
import com.ripple.validator.common.states.PaymentState;
import com.ripple.validator.persistence.converters.ConditionConverter;
import com.ripple.validator.persistence.converters.FulfillmentConverter;
import org.interledger.cryptoconditions.Condition;
import org.interledger.cryptoconditions.Fulfillment;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

import static com.ripple.validator.common.states.PaymentState.PENDING;

/**
 * @author Service Archetype
 */
@Entity
@Table(name = "VALIDATOR_RECORD")
public class ValidatorEntity extends AbstractCreatedModifiedVersioned implements Identifiable<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CORRELATION_ID")
    private UUID correlationId;

    @Column(name = "EXPIRES_DTTM")
    private Instant expiresDttm;

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private PaymentState state;

    @Column(name = "EXECUTION_CONDITION")
    @Convert(converter = ConditionConverter.class)
    private Condition executionCondition;

    @Column(name = "EXECUTION_FULFILLMENT")
    @Convert(converter = FulfillmentConverter.class)
    private Fulfillment executionFulfillment;

    ValidatorEntity() {}

    public ValidatorEntity(UUID correlationId, Instant expiresDttm) {
        this.correlationId = Objects.requireNonNull(correlationId, "correlationId may not be null");
        this.expiresDttm = Objects.requireNonNull(expiresDttm, "expiresDttm may not be null");
        this.state = PENDING;
    }

    @Override
    public Long getId() {
        return id;
    }

    public UUID getCorrelationId() {
        return correlationId;
    }

    public Instant getExpiresDttm() {
        return expiresDttm;
    }

    public void setExpiresDttm(Instant expiresDttm) {
        this.expiresDttm = expiresDttm;
    }

    public PaymentState getState() {
        return state;
    }

    public void setState(PaymentState state) {
        this.state = state;
    }

    public Condition getExecutionCondition() {
        return executionCondition;
    }

    public void setExecutionCondition(Condition executionCondition) {
        this.executionCondition = executionCondition;
    }

    public Fulfillment getExecutionFulfillment() {
        return executionFulfillment;
    }

    public void setExecutionFulfillment(Fulfillment executionFulfillment) {
        this.executionFulfillment = executionFulfillment;
    }
}
