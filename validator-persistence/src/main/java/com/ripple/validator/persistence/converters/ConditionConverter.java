package com.ripple.validator.persistence.converters;

import org.interledger.cryptoconditions.Condition;
import org.interledger.cryptoconditions.CryptoConditionReader;
import org.interledger.cryptoconditions.CryptoConditionWriter;
import org.interledger.cryptoconditions.der.DerEncodingException;

import javax.persistence.AttributeConverter;

/**
 * @author matt
 */
public class ConditionConverter implements AttributeConverter<Condition, byte[]> {
    @Override
    public byte[] convertToDatabaseColumn(Condition condition) {
        if (condition == null) return null;
        try {
            return CryptoConditionWriter.writeCondition(condition);
        } catch (DerEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Condition convertToEntityAttribute(byte[] dbValue) {
        if (dbValue == null) return null;
        try {
            return CryptoConditionReader.readCondition(dbValue);
        } catch(DerEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
