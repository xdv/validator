package com.ripple.validator.persistence.config;


import com.ripple.persistence.SimplePersistenceHealthCheck;
import com.ripple.runtime.RuntimeMode;
import com.ripple.spring.annotations.ConditionalOnLiquibase;
import com.ripple.spring.config.RuntimeModePropertySourceFactory;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 * @author Service Archetype
 */
@Configuration
@EnableJpaRepositories(
    basePackages = {
        "com.ripple.validator.persistence.repositories",
    },
    entityManagerFactoryRef = "validatorEMF",
    transactionManagerRef = "validatorTM")
@Import({
    ValidatorPersistenceH2Config.class,
    ValidatorPersistencePostgresConfig.class,
    ValidatorPersistenceMssqlConfig.class,
    ValidatorPersistenceOracleConfig.class,
})
@PropertySource(value = "classpath:/validator-persistence.properties", factory = RuntimeModePropertySourceFactory.class)
public class ValidatorPersistenceConfig {

    @Autowired
    private Environment env;

    @Bean(name = "validatorTM")
    @Qualifier("validator")
    public JpaTransactionManager validatorTM(
        @Qualifier("validatorDS") final DataSource dataSource,
        @Qualifier("validatorEMF") final EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setDataSource(dataSource);
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean validatorEMF(
        @Qualifier("validatorDS") final DataSource dataSource,
        @Qualifier("validatorVA") final JpaVendorAdapter vendorAdapter) {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource);
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPersistenceUnitName("validator");
        factory.setPackagesToScan(
            "com.ripple.validator.persistence.entities"
        );
        return factory;
    }

    @Bean
    @Qualifier("validator")
    public JdbcTemplate validatorJdbcTemplate(@Qualifier("validatorDS") final DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
        return jdbcTemplate;
    }

    @Bean
    @ConditionalOnLiquibase
    public SpringLiquibase validatorLiquibase(@Qualifier("validatorDS") final DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setContexts(RuntimeMode.getMode().name());
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog("classpath:db/validator/changelog-master.xml");
        return liquibase;
    }

    @Bean
    @Qualifier("validator")
    public SimplePersistenceHealthCheck validatorPersistenceHealthCheck(
        @Qualifier("validator") final JdbcTemplate jdbcTemplate) {
        return new SimplePersistenceHealthCheck(jdbcTemplate, env);
    }
}